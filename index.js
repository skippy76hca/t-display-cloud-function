var fs = require('fs');
var request = require('request');
var moment = require('moment-timezone');
var mysql = require('mysql');


exports.info = (req, res) => {
  console.log("Info function call");
  var debug = true;
  if (process.env.debug == 1) {
    console.log("Global Debug enabled");
  } else {
    debug = false;
    console.log("Global Debug disabled");
  }

  debug && console.log(JSON.stringify(req.body));
  debug && console.log("We have parameters");

  var device = null;

  if (req.body.device != null) {
        debug && console.log("Device ID: " + req.body.device);
        device = req.body.device;

        debug && console.log("Connecting to DB");
        var dbName = process.env.dbName || '';
        debug && console.log("DB name: " + dbName);

        var dbConnectionName = process.env.dbConnectionName || '';
        debug && console.log("DB Connection Name: " + dbConnectionName);

        var dbUsername = process.env.dbUsername || '';
        debug && console.log("DB Username: " + dbUsername);

        var dbPassword = process.env.dbPassword || '';
        //debug && console.log("DB Password: " + dbPassword);

        getProfile(debug, device, dbName, dbUsername, dbPassword, dbConnectionName, function (profile, found) {
            if (found){
                debug && console.log("Got profile");
                var json = JSON.stringify(profile);
                res.writeHead(200, {"Content-Type": "application/json"});
                res.end(json);
            } else {
                res.status(404).send('404 Invalid Device ID: ' + device);
            }
        }); // get profile
  } else  {
        res.status(404).send('404 Invalid Device ID');
  }
}

exports.main = (req, res) => {
  console.log("Main function call");
  var debug = true;
  if (process.env.debug == 1) {
    console.log("Global Debug enabled");
  } else {
    debug = false;
    console.log("Global Debug disabled");
  }

  debug && console.log(JSON.stringify(req.body));

  if (req.body.mode === undefined) {
    var indexfile = process.env.indexfile || "index.html";
    debug && console.log("Displaying html wrapper");

    fs.readFile('./' + indexfile, function (err, html) {
        if (err) {
            throw err; 
        }       
        
        res.writeHeader(200, {"Content-Type": "text/html"});  
        res.write(html);  
        res.end();
    });

  } else {
    debug && console.log("We have parameters");

    var messages = [];
    var device = null;

    if (req.body.device != null) {
        debug && console.log("Device ID: " + req.body.device);
        device = req.body.device;
    }

    debug && console.log("Connecting to DB");
    var dbName = process.env.dbName || '';
    debug && console.log("DB name: " + dbName);

    var dbConnectionName = process.env.dbConnectionName || '';
    debug && console.log("DB Connection Name: " + dbConnectionName);

    var dbUsername = process.env.dbUsername || '';
    debug && console.log("DB Username: " + dbUsername);

    var dbPassword = process.env.dbPassword || '';
    //debug && console.log("DB Password: " + dbPassword);

    getProfile(debug, device, dbName, dbUsername, dbPassword, dbConnectionName, function (profile, found) {
        if (found) {
            var mbtaKey = process.env.mbtaKey || '';
            debug && console.log("MBTA Key: " + mbtaKey);

            var mbtaURL = process.env.mbtaURL || '';
            debug && console.log("MBTA URL: " + mbtaURL);

            // get data from mbta
            // url https://api-v3.mbta.com/predictions?filter%5Bstop%5D=place-sstat&filter%5Bdirection_id%5D=0&include=stop
            var headers = {'x-api-key': mbtaKey };

            var urlib = mbtaURL + 'predictions?sort=arrival_time&filter%5Bstop%5D=' + profile.mbtaStationID + '&filter%5Bdirection_id%5D=0&include=trip&filter%5Broute%5D=Red';
            var urlob = mbtaURL + 'predictions?sort=arrival_time&filter%5Bstop%5D=' + profile.mbtaStationID + '&filter%5Bdirection_id%5D=1&include=trip&filter%5Broute%5D=Red';

            debug && console.log("MBTA Inbound URL: " + urlib);
            debug && console.log("MBTA Outbound URL: " + urlob);

            debug && console.log("Getting inbound data");
            request({url: urlib, headers: headers}, function (error, ibresp, ibdata) {
            if (!error && ibresp.statusCode == 200) {

            debug && console.log("Getting outbound data");

            request({url: urlob, headers: headers}, function (error, obresp, obdata) {
                if (!error && obresp.statusCode == 200) {

                    obdata = JSON.parse(obdata);
                    ibdata = JSON.parse(ibdata);

                    // we care about the next 2 train inbound and outbound
                    var dest
                    var statusib = [];
                    var destib = [];
                    var statusob = [];
                    var destob = [];
                    var clockTime;
                    var counter = 0;

                    if (profile.padTime) {
                        clockTime = moment().tz("America/New_York").format('hh:mm');
                    } else {
                        clockTime = moment().tz("America/New_York").format('h:mm');
                    }

                    for (counter = 0; counter < messages.length; counter++) {
                        messages[counter] = {message: '', color: profile.defaultColor};
                    }

                    // inbound
                    if (ibdata.data.length > 0) {
                        if (profile.timeOffset > 0) {
                            ibdata.data = shiftData(debug, ibdata.data, rofile.timeOffset);
                        }

                        // data received
                        statusib[0] = getStatus(debug, profile.showStatus, ibdata.data[0], getArrival(debug, ibdata.data[0]));
                        destib[0] = getDest(debug, ibdata.data[0].relationships.trip.data.id, ibdata.included);

                        messages[0] = {message: destib[0], color: profile.defaultColor};
                        messages[2] = {message: destib[0], color: profile.defaultColor};

                        if (ibdata.data.length > 1) {    
                            // second arrival time
                            statusib[1] = getStatus(debug, profile.showStatus, ibdata.data[1], getArrival(debug, ibdata.data[1]));
                            destib[1] = getDest(debug, ibdata.data[1].relationships.trip.data.id, ibdata.included);

                            messages[1] = {message: destib[1], color: profile.defaultColor};
                            messages[3] = {message: destib[1], color: profile.defaultColor};
                        }
                    }

                    // outbound
                    if (obdata.data.length > 0) {
                        // outbound messages
                        statusob[0] = getStatus(debug, profile.showStatus, obdata.data[0], getArrival(debug, obdata.data[0]));
                        destob[0] = getDest(debug, obdata.data[0].relationships.trip.data.id, obdata.included);

                        messages[4] = {message: destob[0], color: profile.defaultColor};
                        messages[6] = {message: destob[0], color: profile.defaultColor};
                        
                        if (obdata.data.length > 1) {
                            // second arrival time
                            statusob[1] = getStatus(debug, profile.showStatus, obdata.data[1], getArrival(debug, obdata.data[1]));
                            destob[1] = getDest(debug, obdata.data[1].relationships.trip.data.id, obdata.included);

                            messages[5] = {message: destob[1], color: profile.defaultColor};
                            messages[7] = {message: destob[1], color: profile.defaultColor};
                        }
                    }

                    if (profile.showClock) {
                        // if clock is shown, the status appears aligned after the destination
                        // if clock is shown, the status appears 2 chars after the destib
                        var maxlen = 0;

                        for(let val of messages) {
                            if (maxlen < val.message.length) {
                                maxlen = val.message.length;
                            }
                        }
                        maxlen++;

                        debug && console.log("Max Len: " + maxlen);

                        for (counter = 0; counter < messages.length; counter++) {
                            messages[counter].message = padMessage(messages[counter].message, maxlen);
                        }

                        messages[0].message += statusib[0];
                        messages[1].message += statusib[1];
                        messages[2].message += statusib[0];
                        messages[3].message += statusib[1];
                        messages[4].message += statusob[0];
                        messages[5].message += statusob[1];
                        messages[6].message += statusob[0];
                        messages[7].message += statusob[1];

                        if (profile.clockPosition == 'upper') {
                            messages[0].message = insertRight(messages[0].message, clockTime, profile.messageWidth);
                            messages[1].message = padMessage(messages[1].message, profile.messageWidth);
                            messages[2].message = insertRight(messages[2].message, clockTime, profile.messageWidth);
                            messages[3].message = padMessage(messages[3].message, profile.messageWidth);
                            messages[4].message = insertRight(messages[4].message, clockTime, profile.messageWidth);
                            messages[5].message = padMessage(messages[5].message, profile.messageWidth);
                            messages[6].message = insertRight(messages[6].message, clockTime, profile.messageWidth);
                            messages[7].message = padMessage(messages[7].message, profile.messageWidth);
                        } else {
                            messages[0].message = padMessage(messages[0].message, profile.messageWidth);
                            messages[1].message = insertRight(messages[1].message, clockTime, profile.messageWidth);
                            messages[2].message = padMessage(messages[2].message, profile.messageWidth);
                            messages[3].message = insertRight(messages[3].message, clockTime, profile.messageWidth);
                            messages[4].message = padMessage(messages[4].message, profile.messageWidth);
                            messages[5].message = insertRight(messages[5].message, clockTime, profile.messageWidth);
                            messages[6].message = padMessage(messages[6].message, profile.messageWidth);
                            messages[7].message = insertRight(messages[7].message, clockTime, profile.messageWidth);
                        }
                    } else {
                        // no clock shown, so just align destination left and status right
                        messages[0].message = insertRight(messages[0].message, statusib[0], profile.messageWidth);
                        messages[1].message = insertRight(messages[1].message, statusib[1], profile.messageWidth);
                        messages[2].message = insertRight(messages[2].message, statusib[0], profile.messageWidth);
                        messages[3].message = insertRight(messages[3].message, statusib[1], profile.messageWidth);
                        messages[4].message = insertRight(messages[4].message, statusob[0], profile.messageWidth);
                        messages[5].message = insertRight(messages[5].message, statusob[1], profile.messageWidth);
                        messages[6].message = insertRight(messages[6].message, statusob[0], profile.messageWidth);
                        messages[7].message = insertRight(messages[7].message, statusob[1], profile.messageWidth);
                    }

                    debug && console.log("Mode: " + req.body.mode);

                    switch (req.body.mode) {
                    case 'raw':
                        var counter = 0;
                        var msg = '';		    
                        
                        for (counter = 0; counter < messages.length; counter++) {
                        msg = msg + messages[counter].message + "\n"; 
                        }

                        res.status(200).send(msg);
                        break;

                    case 'json':
                        json = JSON.stringify(messages);
                        res.writeHead(200, {"Content-Type": "application/json"});
                        res.end(json);
                        break;

                    default:
                        res.status(404).send('404 Invalid mode value: ' + req.body.mode);
                    }

                } else {
                    res.status(404).send('404 Failed to get inbound data: ' + error);
                }
            });
            } else {
                res.status(404).send('404 Failed to get inbound data: ' + error);
            }
            });
        } else {
            res.status(404).send('404 Invalid Device ID: ' + device);
        }
    }); // getProfile
  }
};

function getProfile(debug, device, dbName, dbUsername, dbPassword, dbConnectionName, cb) {
    var profile = {showClock: true,
                defaultColor: 'FFA500',
                timeOffset: 0,
                clockPosition: 'upper',
                messageWidth: 20,
                messageHeight: 2,
                padStatus: 11,
                mbtaStationID: 'place-knncl',
                stationID: 0,
                showStatus: true,
                padTime: true,
                firstname: '',
                lastname: ''};
    var found = false;

    const mysqlConfig = {
        connectionLimit: 1,
        user: dbUsername,
        password: dbPassword,
        database: dbName,
        socketPath: `/cloudsql/${dbConnectionName}`
    };

    let mysqlPool;
    mysqlPool = mysql.createPool(mysqlConfig);

    //var sql = "select * from device where device='" + device + "'"
    var sql = "select * from device INNER JOIN devicetype ON device.devicetypeID = devicetype.devicetypeID INNER JOIN account ON device.accountID = account.accountID where device='" + device + "'";
    mysqlPool.query(sql, (err, results) => {
        if (err) throw err;
        debug && console.log("Result: " + JSON.stringify(results));

        if (results.length > 0) {
            found = true;
            debug && console.log("Profile found in DB");
            profile.showClock = results[0].showClock;
            profile.defaultColor = results[0].defaultColor;
            profile.timeOffset = results[0].timeOffset;
            profile.clockPosition = results[0].clockPosition;
            profile.messageWidth = results[0].characters;
            profile.messageHeight = results[0].lines;
            profile.padStatus = results[0].padStatus;
            profile.showStatus = results[0].showStatus;
            profile.padTime = results[0].padTime;
            profile.stationID = results[0].stationID;
            profile.firstname = results[0].firstname;
            profile.lastname = results[0].lastname;

            if (results[0].debug == 1) {
                debug && console.log("User debug on");
                debug = true;
            } else {
                debug && console.log("User debug off");
                debug = false;
            }
        } else {
            debug && console.log("Profile not found, using default");

            if (process.env.showClock == 0) { profile.showClock = false; }
            profile.defaultColor = process.env.defaultColor || '#FFA500';
            profile.timeOffset = process.env.timeOffset || 0;
            profile.clockPosition = process.env.clockPosition || 'upper';
            profile.messageWidth = process.env.messageWidth || 20;
            profile.messageHeight = process.env.messageHeight || 2;
            profile.padStatus = process.env.padStatus || 11;
            profile.mbtaStationID = process.env.mbtaStationID || 'kendall';
            if (process.env.showStatus == 0) { profile.showStatus = false; }
            if (process.env.padTime == 0) { profile.padTime = false; }
        }
        
        debug && console.log("Show clock set to: " +  profile.showClock);
        debug && console.log("Station ID: " + profile.mbtaStationID);
        debug && console.log("Show Status: " + profile.showStatus);
        debug && console.log("Pad time: " + profile.padTime);
        debug && console.log("Pad size for status: " + profile.padStatus);
        debug && console.log("Message width set to: " + profile.messageWidth);
        debug && console.log("Clock position set to: " + profile.clockPosition);
        debug && console.log("Time offset set to: " + profile.timeOffset);
        debug && console.log("Default color set to: " + profile.defaultColor);

        cb(profile, found);
    }); // db uery
}

function padMessage(msg, pad) {
    return msg.padEnd(pad, " ");
}

function shiftData(debug, data, offset) {
    debug && console.log("Doing time offset (mins): " + offset);
    
    // check if the next train is less than 'offset' minutes away
    // we have an arrival time, so calculate difference from now.
    //var tmnow = moment().tz("America/New_York");
    // "2019-08-02T22:12:49-04:00"
    //var tmarr = moment(data.attributes.arrival_time).tz("America/New_York");

    //var duration = moment.duration(tmarr.diff(tmnow));
    //var mins = duration.asMinutes();
    
    //debug && console.log("time diff in mins: " + mins + " - " + Math.round(mins));
    console.log(data);
    return data;
}

function getStatus(debug, showStatus, data, arrtime) {
    debug && console.log("Status: " + data.attributes.status);
    
    if (showStatus == false) {
        return arrtime
    }

    if (data.attributes.status == null) {
        return arrtime;
    }

    // it has a status then like
    // Stopped 5 stops away
    var arr = data.attributes.status.split(" "); 
    var retval = arrtime;

    if (arr.length > 0) {
      retval = arr[1] + " stps awy";
    }

    debug && console.log("Generated status: " + retval);
    return retval;
}

function addZero(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function getArrival(debug, data) {
    // check if no Arrival time, if so boarding
    if (data.attributes.arrival_time === undefined) {
        return "BRD";
    }

    // we have an arrival time, so calculate difference from now.
    var tmnow = moment().tz("America/New_York");
    // "2019-08-02T22:12:49-04:00"
    var tmarr = moment(data.attributes.arrival_time).tz("America/New_York");

    debug && console.log("time now");
    debug && console.log(tmnow);
    debug && console.log("Convert arr");
    debug && console.log(tmarr);

    var duration = moment.duration(tmarr.diff(tmnow));
    var mins = duration.asMinutes();
    
    debug && console.log("time diff in mins: " + mins + " - " + Math.round(mins));

    // if time difference is less than 45 seconds, respond with ARR
    if (mins < 0.25) {
        return "BRD";
    }

    if (mins < 0.75) {
        return "ARR";
    }

    // else round
    return Math.round(mins) + " min";
}

function getDest(debug, tripid, trips) {
    // go throgh the array of trips to match tripid
    // return headingsign
    var counter = 0;
    debug && console.log("Finding dest for trip: " + tripid);

    for (counter = 0; counter < trips.length; counter++) {
        if (trips[counter].id == tripid) {
            return trips[counter].attributes.headsign;
        }
    }

    return '';
}

function insertRight(msg, insert, pad) {
    if (insert.length >= pad) {
        return insert;
    }

    var padmsg = padMessage(msg, pad);

    // need to put the insert in on the aligned right of msg
    // e.g. 
    // msg:  hello..........
    // insert: 8:23
    // msg:  hello......8:23

    var output = padmsg.substring(0, padmsg.length - insert.length);
    return output + insert;

}
